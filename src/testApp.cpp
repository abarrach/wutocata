#include "testApp.h"


//--------------------------------------------------------------
void testApp::setup() {

	ofBackground(0, 0, 0);
	ofSetFrameRate(120);
	ofSetVerticalSync(false);
	bReversePoints	 = false;
	bDrawGray		 = false;
	bDrawBlobs = true;
	bMaInvisible = false;
	simpleAmount     = 1.0;
	threshold		 = 83;
	timed, counter=0;
    status = STAT_NONE;

	vw = 640;
	vh = 480;
//	vw = 320;
//	vh = 240;
	input.initGrabber(vw, vh);
	input.setDesiredFrameRate(60);

	color.allocate(vw, vh);
	colorPlus.allocate(vw, vh, OF_IMAGE_COLOR);
	gray.allocate(vw, vh);
//	contourAnalysis.setSize(vw, vh);
	fons.allocate(vw, vh);

	// open an outgoing connection to HOST:PORT
//	sender.setup( HOST, PORT );

    bSendToSC = false;

    // Roger edit
    sender.setup(HOST,PORT);

}


//--------------------------------------------------------------
void testApp::update() {

/*       if( timed ++ > 100 )
        {
            timed = 0;
            printf( "fps %f\n", ofGetFrameRate() );
        }
*/
//	cout << " " << endl;
	input.update();


	if(input.isFrameNew()) {

//		cout << "_ " << ofGetElapsedTimeMillis()-timed;
		timed = ofGetElapsedTimeMillis();

		color.setFromPixels(input.getPixels(), vw, vh);
		if(bMaInvisible)
		{
			colorPlus.setFromPixels(color.getPixels(),vw, vh,OF_IMAGE_COLOR);
			colorPlus.deleteColor(10, 103, 108, 18, 48, 35, false);
			color.setFromPixels(colorPlus.getPixels(),vw,vh);
		}
		gray = color;
		gray.threshold(threshold, false);

		gray += fons;

		contour.findContours(gray, 30, (vw*vh)/3, MAX_BLOBS, true);

//			cout << contour.nBlobs;
//			cout << " ";
		// for testing lets just grab the largest blob
		for(int j=0; j<contour.nBlobs;j++) {

//			contourAnalysis.simplify(contour.blobs[0].pts, simpleCountour, simpleAmount);
//			cout << "bl_" << contour.nBlobs;
			isSynth_blob_info( &(contour.blobs[j]), &(blobInfo[j]));

			if(blobInfo[j].isSynth)
			{
				blobInfo[j].hue = calc_color(&(contour.blobs[j]), color.getPixels(), gray.getPixels());
				blobInfo[j].isBlack = isBlack(&(contour.blobs[j]), color.getPixels(), gray.getPixels());

				get_blob_info( &(contour.blobs[j]), &(blobInfo[j]));

				synth_count[blobInfo[j].type]++;
				int id = synth_count[blobInfo[j].type]+((blobInfo[j].type+1)*1000);

				// ENVIO INFO A SC via OSC
                // Roger
                sendOsc(id, blobInfo[j]);
			}
		}



		for(int i=0; i<MAX_TYPES;i++) {
			synth_count_old[i] = synth_count[i];
			synth_count[i] = 0;
		}
	}




}

//--------------------------------------------------------------
void testApp::draw() {

	string info;
	ofSetHexColor(0xffffff);
//	contourAnalysis.draw(simpleCountour, 0, 0, vw, vh);

	if(bDrawBlobs)
	{
		gray.draw(0,0);

		for (int i = 0; i < contour.nBlobs; i++){

			ofSetColor(255,255,255);
			if(blobInfo[i].isSynth)
			{
				ofColor rgb;
				if (blobInfo[i].isBlack)
					rgb = Hue2Rgb( blobInfo[i].hue, 255, 0 );
				else
					rgb = Hue2Rgb( blobInfo[i].hue );

				ofFill();
				ofSetColor(255,0,0);
				ofRect(center.x+blobInfo[i].min_pt.x,blobInfo[i].min_pt.y,5, blobInfo[i].max_pt.y-blobInfo[i].min_pt.y);

				ofSetColor(0, 0, 0);
				info = " g:"+ofToString(blobInfo[i].max_pt.y-blobInfo[i].min_pt.y,0);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-7);
				info = " a:"+ofToString(blobInfo[i].area);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-17);
				info = "ap:"+ofToString(blobInfo[i].area_per,3);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-27);
				info = " l:"+ofToString(blobInfo[i].length);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-37);
				info = "gm:"+ofToString(blobInfo[i].gr_medi);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-47);
				info = " t:"+ofToString(blobInfo[i].tightness,3);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-57);
				info = "po:"+ofToString(blobInfo[i].pos,2);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-67);
				info = " p:"+ofToString(blobInfo[i].max_pt.y-center.y,0);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-77);
				info = synthDef[blobInfo[i].type];
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-87);

				info = "h:"+ofToString(blobInfo[i].hue);
				ofDrawBitmapString(info, center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-97);
				ofSetColor(rgb.r, rgb.g, rgb.b);
				ofRect(center.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-123,70,12);
				ofNoFill();
			}
			contour.blobs[i].draw(0,0);
		}
	}else
		color.draw(0,0);

    string s = ofToString(threshold);
    ofDrawBitmapString(s, 20, 20);

    if(status == STAT_NONE)
    {
        ofSetColor(255, 0, 0);
        info = "CENTER THE TURNTABLE & CLICK ON THE AXIS";
        ofDrawBitmapString(info, 200, 240);
    }
    if(status == STAT_CENTER_OK)
    {
        ofSetColor(255, 0, 0);
        info = "CLICK ON ANY POINT ON THE PERIMETER";
        ofDrawBitmapString(info, 200, 240);
    }

}


//--------------------------------------------------------------
void testApp::keyPressed(int key) {


	// gray threshold
	if(key == OF_KEY_UP) {
		threshold ++;
		if(threshold > 255) threshold = 255;
	}
	if(key == OF_KEY_DOWN) {
		threshold --;
		if(threshold < 0) threshold = 0;
	}

/*	// the simple amount
	if(key == OF_KEY_LEFT) {
		simpleAmount += 1.0;
		if(simpleAmount > 100.0) simpleAmount = 100.0;
	}
	if(key == OF_KEY_RIGHT) {
		simpleAmount -= 1.0;
		if(simpleAmount < 1.0) simpleAmount = 1.0;
	}

	// toggle the image draw mode
	if(key == '1') bDrawGray = !bDrawGray;
*/
	if(key == 'v') bDrawBlobs = !bDrawBlobs;
	if(key == 'm') bMaInvisible = !bMaInvisible;

	if(key == 's') 	input.videoSettings();

//	if(key == 'd') 	osc_param(3001, "gate", 0);

	if(key == 'c') {
		center.x = mouseX;
		center.y = mouseY;
		bSendToSC = true;
	}
	if(key == 'r') {
		centerRad = sqrt((mouseX-center.x)*(mouseX-center.x) + (mouseY-center.y)*(mouseY-center.y));
		fons.set_antiTocata(center.x,center.y, centerRad,vw,vh, false);
	}
    if(key == 'o') bSendToSC = true;


}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ) {
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button) {
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button) {
	if(status==STAT_NONE) {
		center.x = x;
		center.y = x;
		bSendToSC = true;
		status = STAT_CENTER_OK;
	}
	else if(status==STAT_CENTER_OK) {
		centerRad = sqrt((x-center.x)*(x-center.x) + (y-center.y)*(y-center.y));
		fons.set_antiTocata(center.x,center.y, centerRad,vw,vh, false);
		status = STAT_RADIUS_OK;
	}

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button) {
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h) {
}

unsigned char testApp::calc_color(ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels) {

    unsigned char hue;
    int counter[256];
	int maxnum=0;

    for(int i=0; i<256; i++)
		   counter[i]=0;

    // compose final result
	for(int i=blob->boundingRect.x; i < blob->boundingRect.x + blob->boundingRect.width; i++) {
//    for(int i=0; i<40; i++){
		for(int j=blob->boundingRect.y; j<blob->boundingRect.y + blob->boundingRect.height; j++){
           int mainPixelPos = (j*vw + i) * 3;		//pixel position of video
           int maskPixelPos = (j*vw + i);		//pixel position of mask

		   if (maskPixels[maskPixelPos]<100) // nom�s els pixels del blob, els negres
		   {
				hue = Rgb2Hue( colorPixels[mainPixelPos], colorPixels[mainPixelPos+1], colorPixels[mainPixelPos+2]);
				counter[hue]++;
		   }

       }
    }

	for(int i=0;i <= 255;i++)
	{
		if(counter[i] > maxnum)
			maxnum=counter[i];
	}
	for(int i=0;i <= 255;i++)
	{
		if(counter[i]==maxnum)
			hue = i;
	}
//		   cout << (int)hue;
//		   cout << " ";
	return hue;
}

bool testApp::isBlack(ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels) {

	bool isBlack;
    unsigned char value;
    int counter[256];
	int maxnum=0;

    for(int i=0; i<256; i++)
		   counter[i]=0;

    // compose final result
	for(int i=blob->boundingRect.x; i < blob->boundingRect.x + blob->boundingRect.width; i++) {
//    for(int i=0; i<40; i++){
		for(int j=blob->boundingRect.y; j<blob->boundingRect.y + blob->boundingRect.height; j++){
           int mainPixelPos = (j*vw + i) * 3;		//pixel position of video
           int maskPixelPos = (j*vw + i);		//pixel position of mask

		   if (maskPixels[maskPixelPos]<100) // nom�s els pixels del blob, els negres
		   {
			   value = Rgb2value( colorPixels[mainPixelPos], colorPixels[mainPixelPos+1], colorPixels[mainPixelPos+2]);
			   counter[value]++;
		   }

       }
    }

	for(int i=0;i <= 255;i++)
	{
		if(counter[i] > maxnum)
			maxnum=counter[i];
	}
	for(int i=0;i <= 255;i++)
	{
		if(counter[i]==maxnum)
			value = i;
	}

	if (value < 27)
		isBlack = true;
	else
		isBlack = false;

	return isBlack;
}

unsigned char testApp::Rgb2Hue(unsigned char r, unsigned char g, unsigned char b) {

		unsigned char hue;
        float max = r;
        if(g > max)
                max = g;
        if(b > max)
                max = b;

        float min = r;
        if(g < min)
                min = g;
        if(b < min)
                min = b;

        if(max == min) { // greys
                hue = 0;
                return hue;
        }

        float h;
        if(r == max) {
                h = ((float) g - (float) b) / (max - min);
                if(h < 0)
                        h += 6.;
        } else if (g == max) {
                h = 2. + ((float) b - (float) r) / (max - min);
        } else {
                h = 4. + ((float) r - (float) g) / (max - min);
        }
        hue = (unsigned char) (255 * h / 6);
		return hue;
}

unsigned char testApp::Rgb2value(unsigned char r, unsigned char g, unsigned char b) {

        float max = r;
        if(g > max)
                max = g;
        if(b > max)
                max = b;

        float min = r;
        if(g < min)
                min = g;
        if(b < min)
                min = b;

        if(max == min) { // greys
			 return (unsigned char)(255 * max);
        }

		return (unsigned char)max;
}

ofColor testApp::Hue2Rgb(unsigned char inH, unsigned char inS, unsigned char inV) {

	ofColor rgb;

	float h = inH * 6. / 255.;
    float s = inS / 255.;
    int hCategory = (int) floorf(h);
    float hRemainder = h - hCategory;
    unsigned char pv = (unsigned char) ((1.f - s) * inV);
    unsigned char qv = (unsigned char) ((1. - s * hRemainder) * inV);
    unsigned char tv = (unsigned char) ((1. - s * (1. - hRemainder)) * inV);

	switch(hCategory) {
           case 0: // r
                 rgb.r = inV;
                 rgb.g = tv;
                 rgb.b = pv;
                 break;
           case 1: // g
                 rgb.r = qv;
                 rgb.g = inV;
                 rgb.b = pv;
                 break;
           case 2:
                 rgb.r = pv;
                 rgb.g = inV;
                 rgb.b = tv;
                 break;
           case 3: // b
                 rgb.r = pv;
                 rgb.g = qv;
                 rgb.b = inV;
                 break;
           case 4:
                 rgb.r = tv;
                 rgb.g = pv;
                 rgb.b = inV;
                 break;
           case 5: // back to r
                 rgb.r = inV;
                 rgb.g = pv;
                 rgb.b = qv;
                 break;
           }
		return rgb;
}

void testApp::isSynth_blob_info( ofxCvBlob * blob, BlobInfo * info){

			int offset = 0;   //default =15
			info->min_pt.y = 2000;
			info->max_pt.y = 0;
			info->isSynth = false;
			bool ptdetectmax = false;
			bool ptdetectmin = false;
			bool amplesuf = false;

//			cout << "in_";
		// is synth?
			do{
				offset=offset+1;
				amplesuf = false;

				for(int i=0; i<blob->nPts; i++) {
					if(blob->pts[i].y > info->max_pt.y && blob->pts[i].x < center.x+offset && blob->pts[i].x > center.x-offset && blob->boundingRect.x < center.x && (blob->boundingRect.x+blob->boundingRect.width)> center.x)
					{
						info->max_pt.y = blob->pts[i].y;
						ptdetectmax = true;
					}
					if(blob->pts[i].y < info->min_pt.y && blob->pts[i].x < center.x+offset && blob->pts[i].x > center.x-offset && blob->boundingRect.x < center.x && (blob->boundingRect.x+blob->boundingRect.width)> center.x)
					{
						info->min_pt.y = blob->pts[i].y;
						ptdetectmin = true;
					}
				}

				if (ptdetectmax && ptdetectmin && (info->max_pt.y - info->min_pt.y)>6)
					amplesuf = true ;

			}while(!amplesuf && offset<45);

			if(ptdetectmax && ptdetectmin && (info->max_pt.y - info->min_pt.y)>0)
			{
				info->isSynth = true;
//				cout << "_Y_";
			}//else
			//	cout << "_N_";

}

void testApp::get_blob_info( ofxCvBlob * blob, BlobInfo * info){

			bool ptdetectmax = false;
			bool ptdetectmin = false;

			// detecta el gruix mitj�
			int count = 0;
			info->gr_medi = 0;
			int off_med = 1;

				for(int j=blob->boundingRect.x; j < blob->boundingRect.x + blob->boundingRect.width; j++) {

					int max_pt=0;
					int min_pt=2000;

					for(int i=0; i<blob->nPts; i++) {
						if(blob->pts[i].y > max_pt && blob->pts[i].x == j)
//						if(blob->pts[i].y > max_pt && blob->pts[i].x < j+off_med && blob->pts[i].x > j-off_med)
						{
							max_pt = blob->pts[i].y;
							ptdetectmax = true;
						}
						if(blob->pts[i].y < min_pt && blob->pts[i].x == j)
//						if(blob->pts[i].y < min_pt && blob->pts[i].x < j+off_med && blob->pts[i].x > j-off_med)
						{
							min_pt = blob->pts[i].y;
							ptdetectmin = true;
						}
					}
					if(ptdetectmax && ptdetectmin && max_pt-min_pt>4)
					{
						info->gr_medi = info->gr_medi + (max_pt-min_pt);
						count++;
					}
				}

				if( count>0)
					info->gr_medi = info->gr_medi/count;
				else
					info->gr_medi = 0;

				// altres valors
				info->tightness = blob->boundingRect.width/blob->boundingRect.height;
				info->area = blob->area;
				info->area_per = (blob->boundingRect.width*blob->boundingRect.height)/blob->area;
				info->length = blob->length;
//				info->area_tight = info->area_per * info->tightness * 1000/info->area;
				info->gruix = info->max_pt.y-info->min_pt.y;
				info->pos = 1.-((center.x-blob->boundingRect.x)/blob->boundingRect.width);

				// TYPE
				bool isRed = info->isBlack==false && (info->hue<15 || info->hue>244);
				bool isRedPure = info->isBlack==false && (info->hue<10 || info->hue>249);
				bool isBlue = info->isBlack==false && info->hue>132 && info->hue<183;
				bool isGreen = info->isBlack==false && info->hue>61 && info->hue<100;
				bool isYellow = info->isBlack==false && info->hue>30 && info->hue<53;

				if (info->area < 80)
				{
					if(isBlue) //|| isRedPure)
					{
//						cout<< "!1 ";
						info->type = ROTU;
					}
					else
					{
//						cout<< "!2 ";
						info->type = GRAIN;
					}
				}
				else if (info->area < 400 ) // area<400 no pot ser objecte
				{
					if(isBlue) // colors de pintura/rotu
					{
							info->type = ROTU;
//							cout<< "!3 ";

					}else if(isRed || isYellow || isGreen)
					{
							info->type = PINT;
//							cout<< "!4 ";
					}
					else
					{
//						cout<< "!5 ";
						info->type = GRAIN;
					}
				}
				else if(info->gr_medi<16)
				{
					if(isBlue)
					{
						if(info->tightness >2.5)
						{
							info->type = ROTU;
//							cout<< "!6 ";
						}
						else if(info->area_per > 2.0)
						{
							info->type = ROTU;
//							cout<< "!7 ";
						}else
						{
							info->type = OBJE;
//							cout<< "!8 ";
						}
					}
					else if(isRed || isYellow || isGreen)
					{
						if(info->tightness >2.1)
						{
							info->type = PINT;
//							cout<< "!8b ";
						}
						else if(info->area < 450)
						{
								info->type = PINT;
//								cout<< "!8c ";
						}
						else if(info->area_per > 2.5)
						{
								info->type = PINT;
//								cout<< "!8d ";
						}else
						{
								info->type = OBJE;
//								cout<< "!8e ";
						}
					}
					else
					{
						info->type = OBJE; //TODO distingir de granuls
//						cout<< "!9 ";
					}
				}
				else if (info->gr_medi > 35) // objectes o pintada vertical/diagonal
				{
					if(isBlue)
					{
						if(info->tightness < 0.4) // vertical
						{
							info->type = ROTU;
//							cout<< "!9a ";
						}
						else if(info->area_per >3.5)
						{
							info->type = ROTU; // forma amb linies verticals
//							cout<< "!9b ";
						}
						else
						{
							info->type = OBJE; // falta pulir
//							cout<< "!9c ";
						}
					}
					else if(isRed || isGreen || isYellow)
					{
						if(info->tightness < 0.4) // vertical
						{
							info->type = PINT;
//							cout<< "!9d ";
						}
						else if(info->area_per >3.5)
						{
							info->type = PINT; // forma amb linies verticals
//							cout<< "!9e ";
						}
						else
						{
							info->type = OBJE; // falta pulir
//							cout<< "!9f ";
						}
					}
					else
					{
						info->type = OBJE; // gran no blau-red
//						cout<< "!10 ";
					}
				}//gruixos 16-45
				else if(isBlue)
				{
					if(info->tightness < 0.5) // pintada vertical
					{
							info->type = ROTU;
//							cout<< "!11 ";
					}
					else if(info->tightness > 2.0)
					{
							info->type = ROTU;
//							cout<< "!12 ";
					}
					else if(info->area_per > 4.0)
					{
						info->type = ROTU;
//						cout<< "!13 ";
					}
					else
					{
						info->type = OBJE;
//						cout<< "!14 "; // falta diferenciar estenedor
					}
				}else if(isRed || isGreen || isYellow)
				{
					if(info->tightness < 0.5) // pintada vertical
					{
							info->type = PINT;
//							cout<< "!15 ";
					}
					else if(info->tightness > 2.0 && info->gr_medi < 24 )
					{
							info->type = PINT;
//							cout<< "!16 ";
					}
					else if(info->area_per > 3.5)
					{
						info->type = PINT;
//						cout<< "!17 ";
					}
					else
					{
						info->type = OBJE;
//						cout<< "!18 "; // falta diferenciar estenedor
					}
				}
				else
				{
					info->type = OBJE; // gran no blau-verd-vermell-groc
//					cout<< "!19 ";
				}
}

// Roger edit
void testApp::sendOsc(int id, BlobInfo &info){
    ofxOscMessage m;
    m.setAddress("/tocata");
    m.addIntArg(id);
    m.addIntArg(ofMap(info.hue,0,255,0.0,1.0));
    m.addIntArg(info.type);
    m.addFloatArg( ofMap(info.min_pt.y,0,ofGetHeight(), 0.0,1.0));
    m.addFloatArg( ofMap(info.max_pt.y,0,ofGetHeight(), 0.0,1.0));
    m.addFloatArg( ofMap(info.gruix,0,ofGetHeight()/2, 0.0,1.0));
    m.addIntArg(info.gr_medi);
    m.addIntArg(info.roughness);
    m.addFloatArg( info.tightness);
    m.addIntArg(info.area);
    m.addFloatArg( info.area_per);
    m.addIntArg(info.isBlack);
    m.addFloatArg( info.pos);

    sender.sendMessage(m);
}
