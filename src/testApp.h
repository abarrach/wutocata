
#pragma once
#include "ofMain.h"
#include "ofxOpenCv.h"
//#include "ofxVectorMath.h"
//#include "ofxCvConvexityDefects.h"
#include "wuGrayImg.h"
#include "wuImagePlus.h"
#include "ofxOsc.h"


#define MAX_BLOBS 200
#define MAX_TYPES 3

#define ROTU 0
#define PINT 1
#define OBJE 2
#define GRAIN 3

// Roger edit
#define HOST "localhost"
#define PORT 57120

typedef struct
{
	bool isSynth;
	ofPoint min_pt;
	ofPoint max_pt;
	int gruix;
	int gr_medi;
	unsigned char hue;
	int type;
	int roughness; // puntiagut o suau-arrodonit
	float tightness; // (inf-1)->estret horitzontal, (1) quadrat, (1-0) estret vertical
	int area;
	float area_per;
	int length;
	bool isBlack;
	float pos;
} BlobInfo;



static string synthDef[] =
{
	"gr_sine",
	"gr_sine",
	"gr_sine",
	"gr_sine"
};

enum {
    STAT_NONE,
    STAT_CENTER_OK,
    STAT_RADIUS_OK,
};

// ------------------------------------------------- App
class testApp : public ofBaseApp {

public:

	void setup();
	void update();
	void draw();

	void keyPressed  (int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);

	unsigned char calc_color(ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels);
	bool isBlack(ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels);
	void get_blob_info( ofxCvBlob * blob, BlobInfo * info);
	void isSynth_blob_info( ofxCvBlob * blob, BlobInfo * info);

    // Roger edit
    void sendOsc(int id, BlobInfo &info);

	unsigned char Rgb2Hue(unsigned char r, unsigned char g, unsigned char b);
	unsigned char Rgb2value(unsigned char r, unsigned char g, unsigned char b);
	ofColor Hue2Rgb(unsigned char inH, unsigned char inS=255, unsigned char inV=128);

    // CS communication
    bool bSendToSC;


	// OpenCV
	bool					bReversePoints;
	bool					bDrawGray;
	bool					bDrawBlobs;
	bool					bMaInvisible;
	float					simpleAmount;
	int						threshold;
	int						vw, vh;
    int                     status;

	// CV images
	ofVideoGrabber			input;
	ofxCvColorImage			color;
	wuImagePlus				colorPlus;
	ofxCvGrayscaleImage		gray;
	wuGrayImg		fons;

	// contour work
	ofxCvContourFinder		contour;
//	ofxCvConvexityDefects	contour;
//	vector <ofPoint>		simpleCountour;

	// tocata position variables
	ofPoint center;
	int centerRad, numSynths;
	int synth_count[3], synth_count_old[3];

	BlobInfo blobInfo[MAX_BLOBS];

	int timed, counter;

    // Roger edit
    // OSC
    ofxOscSender sender;
};
